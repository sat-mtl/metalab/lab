This repository contains public information related to the SAT Metalab research department of the [Society for Arts and Technology](https://sat.qc.ca/).

The static website is generated with [nikola](https://getnikola.com/) and deployed to gitlab pages using the gitlab CI. See file `.gitlab-ci.yml` for installation of `nikola`.

Mark an internship post as closed or filled
----------------------------------------

Edit the post and give it the type `closed` if the internship is closed, or `filled` if the internship has been attributed to a candidate.

Here is an example for the closed status:
```
.. type: closed 
```

And when an internships has been filled, here by an intern called "Jean-Michel Bocal":
```
.. type: filled
.. intern: Jean-Michel Bocal
```


Recommended installation process (python3 venv)
---------------------------------------------
Use python3 venv in the `nikola-venv` directory at the root of this repo

```bash
sudo apt install python3-venv
python3 -m venv nikola-venv
cd nikola-venv/
source bin/activate
bin/python -m pip install -U pip setuptools wheel
bin/python -m pip install -U "Nikola[extras]"
```
Done !

Using venv
----------
Before running the nikola command, activate the python3 venv :
```bash
source nikola-venv/bin/activate
```

Add a page
----------

```
cd webpage
nikola new_page
```
go to the page folder, copy the page in order to get an english version of it
```
cp title-of-my-page.rst title-of-my-page.en.rst
```
Then edit both pages.

Warning: if the date in the post is in the future, the build discards it so it won't appear

See the website locally before pushing to gitlab
------------------------------------------------

From the website folder, run the `nikola auto` to build and run the server automatically detect site changes and rebuild, and refresh the browser:
    
```
cd website
nikola auto --browser
```

This should open the website in your default browser. If not, use this url : `http://127.0.0.1:8000/`


A little about nikola (10 min. read)
--------------

[https://getnikola.com/getting-started.html]()
