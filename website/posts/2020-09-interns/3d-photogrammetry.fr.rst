.. title: Photogrammétrie architecturale par lumière structurée
.. slug: photogrammetrie-3d
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du stage
--------------
Photogrammétrie architecturale par lumière structurée

Objectif
--------
Plusieurs logiciels du Metalab dédiés à l'immersion nécessitent une représentation 3D de l'espace physique. C'est le cas de Splash pour le mapping vidéo, mais aussi de vaRays pour la simulation accoustique.

L'objectif de ce stage est, dans le cas de vaRays, de profiter des capacités de mesure de géométrie par photogrammétrie de Splash pour générer un modèle 3D d'un espace d'écoute. Un cas d'usage sera l'ancien planétarium de Montréal, actuellement occupé par le Centech.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Prendre en charge le processus de reconstruction 3D, de la capture à la reconstruction (avant simplification) avec le logiciel Splash (https://sat-metalab.gitlab.io/splash/) et la librairie Calimiro (https://gitlab.com/sat-metalab/calimiro).
* Définir un processus de capture par photogrammétrie adapté
* Identifier les fonctionnalités manquantes et participer à leur développement
* Participer à la vie du laboratoire
* Documenter le travail et s'assurer de sa reproductibilité
  
Environnement de travail
------------------------
* C++, Python
* Meshroom : https://alicevision.org/#meshroom
* Blender : https://blender.org
* Linux, Python, Free and Open Source Software (FOSS)
