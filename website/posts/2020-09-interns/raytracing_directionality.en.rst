.. title: Add directionality for sources to our audio ray tracing tool (vaRays)
.. slug: directionality_vaRays
.. date: 2022-10-11
.. tags: internships
.. category:
.. link: 
.. description: 
.. type: text

Internship title
----------------
Add directionality for sources to our audio ray tracing tool (vaRays)

Objective
---------
Our audio simulation engine, vaRays, uses ray tracing to simulate acoustics in virtual environments. The objective of this internship will be to propose and implement a method that will allow different possible energy distributions for audio sources in vaRays.

Tasks
------
With the help of the Metalab team:

* Get acquainted with the vaRays code
* Experiment with and propose different methods for the implementation of directionality for audio sources
* Participate in the creation of demonstrations of those implementations
* Document the work and ensure its reproducibility

Work environment
----------------
* Scrums, code review, etc.
* C++
* GitLab, Linux, Free and Open Source Software (FOSS)
* JIRA / Confluence
