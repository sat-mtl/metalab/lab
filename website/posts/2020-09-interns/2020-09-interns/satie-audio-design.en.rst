.. title: Audio design with SATIE for immersive environments
.. slug: audio-design-with-SATIE-for-immersive-environments
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Audio design with SATIE for immersive environments

Objective
---------
SATIE is a software designed for sound spatialization for heterogeneous loudspeaker configurations. Based on the SuperCollider audio programming software, it is also suitable for sound generation and processing. This workshop aims to improve its library of synthetic sounds and effects useful for demonstrations or to inspire productions. At the same time find production pipelines that allow the cooperation of arbitrary tools with SATIE.

Tasks
-----
* Participate in the ideation
* Propose and develop synthesis and effects plugins for SATIE with short examples.
* In collaboration with the Metalab team and the production team, identify opportunities for flexible production pipelines
* Participate in the development of a demo (video or live)
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* Linux, free tools
* SuperCollider, git, JACK, audio ecosystem under Linux
* Potentially Bash, Python
