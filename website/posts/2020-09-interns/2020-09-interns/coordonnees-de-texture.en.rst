.. title: Automatic generation of texture coordinates for projection mapping
.. slug: automatic-generation-of-texture-coordinates-for-projection-mapping
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Automatic generation of texture coordinates for projection mapping

Obje3ctive
----------
Projection mapping installations require that the content delivered matches the physical environment. It is the calibration phase of the display devices that allows the video projections to be placed precisely on the immersive surface. The objective of this course is an essential step in the calibration process. It is to develop a prototype that from a mesh of a projection surface calculates texture coordinates according to a user's point of view.

Tasks
-----
* Exploring texture coordinate generation techniques for our use case
* Work with point clouds representing projection surfaces such as a dome, half-dome, plane, or other geometries asbtraites
* Develop and evaluate prototypes
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* C++ / Python
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
