.. title: Custom audio rendering with Mozilla Hubs, an environment for social virtual reality in the browser
.. slug: mozilla-hubs-audio
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------

Custom audio rendering with Mozilla Hubs, an environment for social virtual reality in the browser.

Objective
---------
Analyze the audio transmission logic of Mozilla Hubs and evaluate the possibility of modifying its spatialized audio rendering engine.

Tasks
-----
With the help of the Metalab team :

* Produce an analysis of the code and behavior of the open source platform Mozilla Hubs
* Propose possible approaches to modify and replace the Mozilla Hubs spatialization system.
* Prototyping modifications of the spatialization engine
* Participate in the production of a video demonstration for the end of the course.
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* Mozilla Hubs https://hubs.mozilla.com/
* Javascript, nodeJS, Amazon AWS, git
* GitLab, Python, Bash
* Linux, free tools
