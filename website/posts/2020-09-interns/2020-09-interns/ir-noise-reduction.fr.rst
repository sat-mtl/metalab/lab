.. title: Réduction de bruit pour la simulation d'acoustique par lancer de rayons
.. slug: reduction-de-bruit-dans-les-ri
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed

Titre du stage
--------------
Réduction de bruit pour la simulation d'acoustique par lancer de rayons

Objectif
--------

Le Metalab développe vaRays, un moteur de lancer de rayons sonores permettant de simuler la réponse acoustique (ou réponse impulsionnelle, IR) d'un lieu selon sa géométrie et les matériaux qui le composent. La qualité de la simulation est directement liée au nombre de rayons lancés et la simulation doit être faite pour chaque couple auditeur / source sonore, ce qui peut mener à une quantité de calculs très importante. Une piste d'amélioration consisterait à ajouter une passe de réduction du bruit sur les IRs afin de conserver un nombre de rayons à envoyer raisonnable. Les approches modernes dans le domaine du rendu visuel mettent en œuvre l'apprentissage profond avec d'excellents résultats.

L'objectif de ce stage est donc d'explorer l'application de l'apprentissage profond pour réduire le bruit des IRs et améliorer la qualité de la simulation avec un nombre donné de rayons lancés.

Tâches
------
Avec le soutien de l'équipe du Metalab:

* Prendre connaissance de l'API Python de vaRays
* Faire un état de l'art des méthodes de réduction de bruit pour les IRs
* Faire un état de l'art des méthodes de réduction de bruit pour le rendu visuel, et évaluer la transposition à la simulation acoustique
* Implémenter et expérimenter des méthodes de réduction de bruit pour les IRs
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Python, Tensorflow, PyTorch
* Traitement numérique des signaux
* Gitlab, JIRA / Confluence
* Linux

Reférences :

* sat-metalab.gitlab.io/varays
* https://www.openimagedenoise.org/
