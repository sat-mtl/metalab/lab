.. title: Add support of VR headsets in Metalab's visual immersion tools
.. slug: support-of-vr-headsets
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Add support of VR headsets in Metalab's visual immersion tools

Objective
---------
Metalab's tools enable the deployment of immersive visual spaces, in particular with Splash (https://gitlab.com/sat-metalab/splash) and EiS (https://gitlab.com/sat-metalab/EditionInSitu). However, these tools are dedicated to group immersion, which requires a place in which to use them. By adding the support of VR headsets to these two softwares, it becomes possible to test an immersive space before physically deploying it.

Tasks
-----
* Become familiar with the OpenXR standard and related tools
* Participate in the implementation of OpenXR in Splash and EiS
* Participate in the creation of demonstrations of these implementations
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* C++, Python
* OpenGL, GLSL, OpenXR
* Linux, Free and Open Source Software (FOSS)
