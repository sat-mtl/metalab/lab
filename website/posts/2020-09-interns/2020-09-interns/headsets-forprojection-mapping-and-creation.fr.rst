.. title: Ajout du support de casques de RV dans les outils d'immersion visuelle du Metalab
.. slug: casques-de-rv-
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du stage
--------------
Ajout du support de casques de RV dans les outils d'immersion visuelle du Metalab

Objectif
--------
Les outils du Metalab permettent de déployer des espaces immersifs visuels, en particulier avec Splash (https://gitlab.com/sat-metalab/splash) et EiS (https://gitlab.com/sat-metalab/EditionInSitu). Ces outils sont cependant dédiés à de l'immersion de groupe ce qui oblige à avoir un lieu dans lequel les utiliser. En ajoutant le support des casques de RV à ces deux logiciels, il devient envisageable de tester un espace immersif avant de le déployer physiquement.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Se familiariser avec le standard OpenXR et les outils qui y sont liés
* Participer à l'implémentation de OpenXR dans Splash et EiS
* Participer à la création de démonstration de ces implémentations
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* C++, Python
* OpenGL, GLSL, OpenXR
* Linux, Free and Open Source Software (FOSS)
