.. title: Amélioration de la chaîne colorimétrique de notre outil de projection mapping (Splash)
.. slug: chaine-colorimétrique-splash
.. date: 2022-01-10
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: filled
.. author: Emmanuel Durand
.. intern: Éva Décorps
.. mentors: Christian Frisson, Emmanuel Durand


Titre du stage
--------------
Amélioration de la chaîne colorimétrique de notre outil de projection mapping (Splash)

Objectif
--------
La qualité visuelle d'un système de projection impliquant plusieurs vidéo-projecteurs, en faisant usage de techniques de projection mapping, peut mener à des différences visibles entre les projecteurs malgré un calibrage géométrique excellent. Cela peut être dû à la surface de projection, à des différences physiques entre les projecteurs, à des variations dans leurs installations, etc. De plus les dispositifs de projection mapping montrent en général des faiblesses lorsqu'il s'agit de diffuser des contenus sombres du fait que les projecteurs ne projettent pas un noir total. Les zones de recouvrement entre les projections deviennent alors clairement visibles, ce qui diminue la qualité visuelle et le cas échéant l'immersion de l'utilisateur.

Ce stage a donc trois objectifs :

* s'assurer de la cohérence de la chaîne colorimétrique de Splash, et éventuellement l'améliorer
* améliorer la prise en charge de la colorimétrie et mettre en œuvre des méthode de calibrage colorimétrique modernes dans Splash
* améliorer la prise en charge des zones de recouvrement (blending) notamment en mettant en œuvre des méthodes de diffusion (ou dithering) des couleurs dans le cas d'un paramétrage variable du niveau de noir.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* prendre connaissance du code de Splash
* participer à la documentation du fonctionnement de la chaîne colorimétrique de Splash
* expérimenter le calibrage colorimétrique tel qu'existant, proposer et implémenter des améliorations
* expérimenter la prise en charge des zones de recouvrement dans le cas de contenu sombre, proposer et implémenter des améliorations
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* C++ / OpenGL / OpenCV
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)

Reférences :

* sat-metalab.gitlab.io/splash
* https://en.wikipedia.org/wiki/Black_level
* https://en.wikipedia.org/wiki/Dither
* https://www.arnoldrenderer.com/research/dither_abstract.pdf
