.. title: Création d'une extension audio pour glTF, un format de scène 3D 
.. slug: extension-audio-gltf
.. date: 2022-10-11
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: text


Titre du stage
--------------
Définition et implémentation d'une extension audio pour le format d'échange de scène 3D glTF

Objectif
--------
Le format glTF est un standard normalisé (https://github.com/KhronosGroup/glTF) permettant l'échange de scène 3D complexes incluant modèles 3D, animations, caméras, etc. En revanche ce format ne prend cependant pas en charge la partie audio. Il s'agit donc de définir une extension dédiée permettant de décrire les aspects sonores d'une scène 3D.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Faire un état de l'art des formats de description de scène audio
* Participer à la définition des spécifications d'une extension audio au format glTF
* Développer un exemple d'implémentation
* Ajouter le support de l'extension dans les outils développés/utilisés par le Metalab : EiS, vaRays, Blender
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Blender : https://blender.org
* Linux, Python, C++, Free and Open Source Software (FOSS)
