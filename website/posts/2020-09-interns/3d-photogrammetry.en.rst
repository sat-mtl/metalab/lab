.. title: 3D photogrammetry
.. slug: 3d-photogrammetry
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Architectural photogrammetry using structured light

Objective
---------
Several of Metalab's software programs dedicated to immersion require a 3D representation of the physical space. This is the case of Splash for video mapping, but also of vaRays for acoustic simulation.

The objective of this internship is, in the case of vaRays, to take advantage of Splash's geometry measurement capabilities by photogrammetry to generate a 3D model of a listening space. A use case will be the former planetarium of Montreal, currently occupied by the Centech.

Tasks
-----
With the support of the Metalab team:

* Take charge of the 3D reconstruction process, from capture to reconstruction (before simplification) with the Splash software (https://sat-metalab.gitlab.io/splash/) and the Calimiro library (https://gitlab.com/sat-metalab/calimiro).
* Define an adapted photogrammetric capture process
* Identify missing functionalities and participate in their development
* Participate in the life of the laboratory
* Document the work and ensure its reproducibility

Work environment
----------------
* C++, Python
* Meshroom : https://alicevision.org/#meshroom
* Blender : https://blender.org
* Linux, Python, Free and Open Source Software (FOSS)
