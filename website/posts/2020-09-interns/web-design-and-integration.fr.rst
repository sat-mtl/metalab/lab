.. title: SATIE - Web design et integration
.. slug: Web-design-and-integration
.. date: 2020-09-24 15:00:00 UTC-04:00
.. tags: stages   
.. category:
.. link: 
.. description: 
.. type: closed



Titre du Stage
--------------

Conception d'expérience utilisateur et intégration pour application web dédiée à la spatialisation du son.

Objectif
--------
Participer à la conception et l'amélioration de l'expérience utilisateur de l'interface web de SATIE, notre logiciel de spatialisation audio.
Tâches

Tâches
------
Avec le soutien de l'équipe du Metalab:

* Analyser l'expérience de calibrage de système de haut-parleur la version présente de l'interface (casque, Satosphère, speaker dodécaèdre, plancher haptique)
* Proposer un ou plusieurs modèles d'interface
* Amélioration de l'interface
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* React, SocketIO, Node.js, Javascript, Python, SuperCollider, OSC, Git
* GitLab, Bash, Linux, outils libres
* https://gitlab.com/sat-metalab/poire
* https://gitlab.com/sat-metalab/SATIE
* https://vimeo.com/265607999
* https://vimeo.com/290925507
