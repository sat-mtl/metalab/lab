.. title: Computer Graphics and Computer Vision Programming for the Technological Arts
.. slug: prog-cg-cv-cpp-python
.. date: 2021-06-21 17:00:00 UTC-04:00
.. tags: jobs 
.. category:
.. link: 
.. description: 
.. type: closed

Position
--------

The Society for Arts and Technology [SAT] is looking for a developer-researcher in computer graphics and computer vision to start immediately in its research laboratory. Your role will be to participate in the research process: monitoring, ideation, development, implementation, communication and partnership projects. In particular, you will participate in the work surrounding our video mapper (Splash), our structured light photogrammetry library (Calimiro) and our multi-camera pose estimation tool (Spook).


Technical environment
---------------------

* Ubuntu, Linux, FOSS
* OpenGL, OpenCV, Tensorflow, PyTorch, Numpy
* C++, CMake, Python
* Doxygen, Sphinx
* NVIDIA Jetson
* Gitlab, JIRA, Confluence, Slack

Assets
------
* Real-time 3D programming
* Image processing
* Machine learning
* Interactive installations
* Implementation and redaction of scientific articles

Work environment
----------------

Founded in 2002, the `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__ is the Society for Arts and Technology [`SAT <https://sat.qc.ca/>`_]’s research and development lab. The Metalab has a dual mission: to stimulate the emergence of new immersive experiences, and to make their design and authoring accessible to artists and creators through an ecosystem of free software.

The Metalab's research fields – telepresence, immersion, video mapping, and spatialized sound – are explored through research and production projects. The Metalab team is comprised of 8 to 15 people with various technical expertise including digital audio, computer graphics, networks, software development and multimedia integration. The Metalab welcomes interns in a variety of subjects, even outside the strict scope of the laboratory expertise.

The Metalab's work aims to cover the whole pipeline of immersive and distributed content production. The Metalab’s software addresses, among others, immersive in situ editing, video mapping, audio spatialization, group interaction and low latency transmission. They are designed as flexible tools, and support Metalab’s research and partnerships. This software suite is used by the SAT during creative residencies and is at the heart of telepresence projects, between libraries (`Bibliolab <https://sat.qc.ca/fr/bibliolab>`_) and between venues (`Scènes ouvertes <https://sat.qc.ca/fr/scenes-ouvertes>`_). Metalab’s partners include artists, university researchers, schools, designers and private companies.

Metalab Software
--------------------

* Immersive Space Editing and Prototyping: `In situ editing (EiS) <https://gitlab.com/sat-metalab/EditionInSitu>`_
* Multi-projector video mapping for all types of surfaces: `Splash <https://gitlab.com/sat-metalab/splash/-/wikis/home>`_
* Calibration of projectors for immersive space: `Calimiro <https://gitlab.com/sat-metalab/calimiro>`_
* Sound spatialization for all types of speaker systems: `SATIE <https://gitlab.com/sat-metalab/SATIE>`_
* Sound spatialization by acoustic simulation: `vaRays <https://gitlab.com/sat-metalab/varays>`_
* Pose estimation of multiple people in an immersive space: `Spook <https://gitlab.com/sat-metalab/spook>`_
* Telepresence and multichannel transmission in low latency: `Switcher <https://gitlab.com/sat-metalab/switcher>`_
* Sharing of all types of data streams between applications: `Shmdata <https://gitlab.com/sat-metalab/shmdata>`_
