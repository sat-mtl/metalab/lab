.. title: Programmation informatique graphique et vision par ordinateur pour les Arts Technologiques 
.. slug: prog-cg-cv-cpp-python
.. date: 2021-06-21 17:00:00 UTC-04:00
.. tags: emplois 
.. category:
.. link: 
.. description: 
.. type: closed

Poste
-----

La Société des arts technologiques [SAT] recherche, pour une entrée en fonction immédiate au sein de son laboratoire de recherche, un.e développeur.euse chercheur.euse en informatique graphique et en vision par ordinateur. Ton rôle sera de participer au processus de recherche : veille, idéation, développement, mise en œuvre, communication et projets en partenariats. Tu participeras en particulier aux travaux entourant notre *video mapper* (Splash), notre librairie de photogramétrie par lumière structurée (Calimiro) et notre outil de détection de pose à partir de multiples caméras (Spook).


Environnement technique
-----------------------

* Ubuntu, Linux, FOSS
* OpenGL, OpenCV, Tensorflow, PyTorch, Numpy
* C++, CMake, Python
* Doxygen, Sphinx
* NVIDIA Jetson
* Gitlab, JIRA, Confluence, Slack

Atouts
------
* Programmation 3D temps réel
* Traitement d'image
* Apprentissage machine
* Installations interactives
* Implémentation et rédaction d'articles scientifiques

Environnement de travail
------------------------

Formé depuis 2002, le `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__ est le laboratoire de recherche et développement de la Société des arts technologiques [`SAT <https://sat.qc.ca/>`_]. La mission du Metalab est double: stimuler l'émergence de d'expériences immersives innovantes et rendre leur conception accessible aux artistes et aux créateurs de l'immersion à travers un écosystème de logiciels libres.

Les thématiques de recherche du Metalab — la téléprésence, l'immersion, le mapping vidéo et le son spatialisé — sont développées à travers des projets de recherche et de production. L'équipe du Metalab compte entre 8 et 15 personnes et regroupe des expertises techniques variées : audio numérique, informatique graphique, réseaux, développement logiciel et intégration multimédia. Cette équipe accueille des stagiaires, y compris dans d'autres domaines de compétence.

L'écosystème de logiciels libres du Metalab a pour ambition de couvrir l'intégralité de la production de contenu immersif et distribué: l'édition d'environnement immersif, le video mapping, la spatialisation du son, l'interaction de groupe et la transmission basse latence. Ils sont développés pour être évolutifs sur le long terme: ils sont le support des recherches et des partenariats. En interne à la SAT, ces logiciels sont intégrés à la palette d'outils des résidences de création de la [SAT] et sont au cœur des projets de téléprésence, entre bibliothèques (`Bibliolab <https://sat.qc.ca/fr/bibliolab>`_) et entre salles de spectacle (`Scènes ouvertes <https://sat.qc.ca/fr/scenes-ouvertes>`_). Nos partenaires comptent notamment des artistes, des chercheurs universitaires, des écoles, des designers et des industriels.

Logiciel du Metalab
-------------------

* Téléprésence et transmission multicanal en basse latence : `Switcher <https://gitlab.com/sat-metalab/switcher>`_
* Partage de tous types de flux de données entre applications : `Shmdata <https://gitlab.com/sat-metalab/shmdata>`_
* Video mapping multi-projecteurs pour tous types de surface : `Splash <https://gitlab.com/sat-metalab/splash/-/wikis/home>`_
* Calibrage de projecteurs pour espaces immersif : `Calimiro <https://gitlab.com/sat-metalab/calimiro>`_
* Spatialisation du son pour tous types de système de haut-parleurs : `SATIE <https://gitlab.com/sat-metalab/SATIE>`_
* Spatialisation du son par simulation acoustique : `vaRays <https://gitlab.com/sat-metalab/varays>`_
* Détection de la pose de multiples personnes en environnement immersif : `Spook <https://gitlab.com/sat-metalab/spook>`_
* Édition et prototypage d'espace immersifs : `édition in situ (EiS) <https://gitlab.com/sat-metalab/EditionInSitu>`_
