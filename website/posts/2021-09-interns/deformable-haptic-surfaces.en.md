<!--
.. title: Development of an open-source toolkit for creating deformable haptic surfaces
.. slug: deformable-haptic-surfaces
.. date: 2022-01-10
.. tags: internships, haptics, deformable, surfaces, Splash
.. category: haptics
.. link:
.. description:
.. type: filled
.. author: Christian Frisson
.. intern: Raphaël Suzor-Schröder
.. mentors: Christian Frisson, Nicolas Bouillot
-->

# Internship title

Development of an open-source toolkit for creating deformable haptic surfaces

# Context

Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. [Metalab](https://sat-metalab.gitlab.io)'s mission is twofold: 

1. to stimulate the emergence of innovative immersive experiences, 
2. to make their design accessible to artists and creators of immersion through an ecosystem of free software. 

Besides our set of tools for audio- and visual rendering, we are exploring ways to enhance immersive experiences with haptic feedback, to create simulations that stimulate the sense of touch. One first haptic rendering device created at Metalab is the [Scalable Haptic Floor](https://sat.qc.ca/en/nouvelles/sat-scalable-haptic-floor) that transmits haptic feedback to our bodies through their points of contact with the floor. In addition, deformable haptic surfaces render haptic feedback through peripheral surfaces above the ground [[1](#1)], opening new perspectives for immersion through interactive haptic feedback beyond desk-based settings [[2](#2)].

# Objective

The main objective of this internship is to develop an open-source toolkit for creating haptic feedback on deformable surfaces, reusing recent research works [[1](#1)], and facilitating the authoring of deformable haptic interactions for artists and creators of immersion.

This toolkit could complement other [Metalab](https://sat-metalab.gitlab.io) tools, for instance informing [Splash](https://sat-metalab.gitlab.io/documentations/splash/) of surface deformations for video mapping on deformable surfaces.

# Tasks

- Review related actuation techniques. 
- Develop the open-source toolkit (hardware and software).
- Create a demo video.
- Participate in the life of the laboratory: scrums, code review, etc.
- Document our work in a lab notebook and git repositories and ensure its reproducibility.
- Co-author a publication in the scope of this internship.

# Work environment

- Hardware deployment: Arduino or Raspberry Pi
- Software development: C++, JavaScript or Python
- Documentation: BibTeX, LaTeX, Markdown
- Workflows: Confluence, Gitlab, Jira
- Ecosystem: Linux, Free and Open Source Software (FOSS)

# References

- <a name="1">[1]<a> Anthony Steed et al., “A Mechatronic Shape Display Based on Auxetic Materials,” <i>Nature Communications</i> 12, no. 1 (August 6, 2021): 4758, <a href="https://doi.org/10.1038/s41467-021-24974-0">DOI: 10.1038/s41467-021-24974-0</a>.
- <a name="2">[2]<a> Y. Jansen and P. Dragicevic, “An Interaction Model for Visualizations Beyond The Desktop,” <i>IEEE Transactions on Visualization and Computer Graphics</i> 19, no. 12 (December 2013): 2396–2405, <a href="https://doi.org/10.1109/TVCG.2013.134">DOI: 10.1109/TVCG.2013.134</a>. 
[HAL](https://hal.inria.fr/hal-00847218v2)