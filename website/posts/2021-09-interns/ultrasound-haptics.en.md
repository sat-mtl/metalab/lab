<!--
.. title: Development of an open-source toolkit for creating haptic effects with ultrasound displays
.. slug: ultrasound-haptics
.. date: 2022-10-11
.. tags: internships, haptics, ultrasound
.. category: haptics
.. link:
.. description:
.. type: filled
.. intern: Ezra Pierce
.. mentors: Christian Frisson, Edu Meneses, Michał Seta
.. author: Christian Frisson
-->

# Internship title

Development of an open-source toolkit for creating haptic effects with ultrasound displays

# Context 

Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. [Metalab](https://sat-metalab.gitlab.io)'s mission is twofold: 

1. to stimulate the emergence of innovative immersive experiences, 
2. to make their design accessible to artists and creators of immersion through an ecosystem of free software. 

Besides our set of tools for audio- and visual rendering, we are exploring ways to enhance immersive experiences with haptic feedback, to create simulations that stimulate the sense of touch. One first haptic rendering device created at Metalab is the [Scalable Haptic Floor](https://sat.qc.ca/en/nouvelles/sat-scalable-haptic-floor) that transmits haptic feedback to our bodies through their points of contact with the floor. In addition, ultrasound haptics render air-borne haptic feedback in mid-air, without contact, and without requiring to wear devices [[1](#1),[2](#2)], opening new perspectives for immersion through focused haptic feedback.

# Objective

The main objective of this internship is to develop an open-source toolkit for creating haptic feedback with ultrasound displays, reusing recent research works [[1](#1),[2](#2)], and facilitating the authoring of ultrasound haptic effects for artists and creators of immersion.

This toolkit could complement other [Metalab](https://sat-metalab.gitlab.io) tools, for instance using [Poire](https://gitlab.com/sat-metalab/poire) for haptic generation through audio signals as proxy, similarly to the [Scalable Haptic Floor](https://sat.qc.ca/en/nouvelles/sat-scalable-haptic-floor), and [LivePose](https://sat-metalab.gitlab.io/documentations/livepose/) for posture or hand detection to support interactive scenarios.

# Tasks

- Review haptic effects that can be generated with ultrasound displays.
- Develop the open-source toolkit (hardware and software building upon recent research works [[1](#1),[2](#2)]).
- Create a demo video.
- Participate in the life of the laboratory: scrums, code review, etc.
- Document our work in a lab notebook and git repositories and ensure its reproducibility.
- Co-author a publication in the scope of this internship.

# Work environment

- Hardware deployment: Arduino or Raspberry Pi
- Software development: C++, JavaScript or Python
- Documentation: BibTeX, LaTeX, Markdown
- Workflows: Confluence, Gitlab, Jira
- Ecosystem: Linux, Free and Open Source Software (FOSS)

# References

- <a name="1">[1]<a> Asier Marzo, Tom Corkett, and Bruce W. Drinkwater, “Ultraino: An Open Phased-Array System for Narrowband Airborne Ultrasound Transmission,” <i>IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control</i> 65, no. 1 (January 2018): 102–11, <a href="https://doi.org/10.1109/TUFFC.2017.2769399">DOI: 10.1109/TUFFC.2017.2769399</a>. 
Materials: 
[github](https://github.com/asiermarzo/Ultraino/) 
[instructables](https://www.instructables.com/Ultrasonic-Array/) 
[makerfabs](https://www.makerfabs.com/index.php?route=product/product&product_id=508)  
- <a name="2">[2]<a> Rafael Morales et al., “Generating Airborne Ultrasonic Amplitude Patterns Using an Open Hardware Phased Array,” <i>Applied Sciences</i> 11, no. 7 (January 2021): 2981, <a href="https://doi.org/10.3390/app11072981">DOI: 10.3390/app11072981</a>. 
Materials: 
[github](https://github.com/upnalab/SonicSurface)
