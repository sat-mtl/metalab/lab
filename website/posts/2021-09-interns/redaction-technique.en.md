<!--
.. title: Technical writing
.. slug: documentation
.. date: 2022-02-16 15:00:00 UTC-04:00
.. tags: internships
.. category: documentation
.. link: 
.. description: 
.. type: closed
-->

# Internship title

Technical writing

## Goal

You like learning new technologies? Moreso, you like explaining them to others? Searching for the right word does not scare you? We would like to meet you!

The goal of the technical writing internship is to play an active role in creating and writing new tutorials for at least one of the following software projects: Splash, shmdata and LivePose.

The Metalab team write, edit and maintain documentation websites to facilitate the discovery and use of their tools. 

## Tasks

With the support of the Metalab team:

* Explore the Metalab tools by using them with their documentation, with the goal of understanding their main usecases.
* Identify usecases to be documented, with the help of a Metalab team member.
* Make a plan for the new tutorials to be written.
* Write the new tutorials.
* Improve the tutorials according to comments by the team.

## Work environment

For the documentation, our main tools are Sphinx (markdown or rst format) and GitLab.

Our environment promotes team work and the adoption of open source software, including:

* Scrums and code reviews
* GitLab, Markdown
* Linux, Free and Open Source Software (FOSS)
* LaTeX
