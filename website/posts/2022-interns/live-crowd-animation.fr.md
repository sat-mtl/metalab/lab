<!--
.. title: Animation de foule en direct
.. slug: animation-de-foule-en-direct
.. date: 2022-10-11
.. tags: stages
.. category: 3d
.. link:
.. description:
.. type: filled
.. author: Nicolas Bouillot
.. intern: Manuel Bolduc
.. mentors: Christian Frisson, Emmanuel Durand
-->

# Titre du stage
Animation de foule en direct

# Objectif
Dans le contexte de virtualisation en direct de concert symphonique, une captation vidéo des artistes et du public permet d'extraire des informations de mouvement de la foule. L'objectif de ce stage est de prototyper une approche simple permettant de modéliser en temps réel l'activité de la salle de concert dans un environnement 3d en ligne.


# Tâches

- Participer à l'idéation de l'approche.
- Implémenter l'approche, en collaboration avec les équipes de recherche et développement.
- Participer à une captation d'orchestre symphonique
- Modéliser en 3d des avatars des personnes présentes (artistes et public)
- Élaborer des stratégies d'animation
- Participer et accompagner l'intégration du système d'animation dans un environnement 3d en ligne


# Contexte et logiciels

- [LivePose](https://gitlab.com/sat-mtl/tools/livepose)
- Mapping de flux de données
- Mozilla Hubs à travers l'instance de la SAT, appelée [Satellite](https://sat.qc.ca/fr/satellite#section)
- Blender
