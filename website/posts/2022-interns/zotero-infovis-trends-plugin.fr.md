<!--
.. title: Développement d'une extension d'un outil de gestion de bibliographies pour faciliter l'exploration de tendances
.. slug: zotero-infovis-trends-plugin
.. date: 2022-10-11
.. tags: stages
.. category: 
.. link:
.. description:
.. type: filled
.. author: Christian Frisson
.. intern: Yassin Akbib
.. mentors: Christian Frisson
-->

# Titre du stage

Développement d'une extension d'un outil de gestion de bibliographies pour faciliter l'exploration de tendances

# Objectif

Au Metalab et dans d'autres départements pour l'innovation de la SAT, comme dans bien d'autres groupes de Recherche et Développement, nous produisons régulièrement des veilles technologiques pour nous maintenir à jour sur les progrès technologiques récents, dans notre cas pour la création d'expériences immersives multimedia. Nous utilisons [Zotero](https://www.zotero.org) pour partager des collections d'éléments bibliographiques.

Quelques extensions Zotero ont été créées pour assister des tâches d'exploration de tendances sur des collections à l'aide de visualisation d'information et de fouille textuelle, mais elles sont soit obsolètes ([Paper Machines](https://www.papermachines.org), [zotero-voyant-export](https://github.com/corajr/zotero-voyant-export) pour [Voyant Tools](https://voyant-tools.org)), soit [en cours](https://github.com/search?q=zotero+visualization&ref=opensearch). 

Le but de ce stage est de développer une extension pour Zotero pour faciliter l'exploration de tendances, d'abord pour des collections de fichiers PDF. Restaurer des extensions obsolètes ou contribuer à des extensions en cours ou démarrer de zéro sont des options envisageables.

# Tâches

- Passer en revue les extensions Zotero existantes 
- Comprendre les techniques de visualisation d'information et de fouille de texte qui facilitent les tâches d'exploration de tendances
- Développer une extension (restaurer des extensions obsolètes ou contribuer à des extensions en cours ou démarrer de zéro)
* (Bonus) Présenter nos contributions dans une conférence internationale

# Contexte et logiciels

- [Development d'extensions](https://www.zotero.org/support/dev/client_coding/extension_development) pour [Zotero](https://www.zotero.org): JavaScript, CSS, technologies Mozilla (XPCOM, XULRunner, XUL, etc.), Git
- [Extensions Zotero existantes](https://www.zotero.org/support/extensions): [Paper Machines](https://www.papermachines.org), [zotero-voyant-export](https://github.com/corajr/zotero-voyant-export) pour [Voyant Tools](https://voyant-tools.org), et [bien d'autres](https://github.com/search?q=zotero+visualization&ref=opensearch)...
