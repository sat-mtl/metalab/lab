<!--
.. title: Develop a user-oriented interface for Splash, our video-mapping software
.. slug: user-oriented-splash-ui
.. author: Emmanuel Durand
.. date: 2022-09-18 10:19:11 UTC-05:00
.. tags: internships, imgui, Splash, gui
.. link:
.. description:
.. type: text
.. mentors: Emmanuel Durand
-->

# Intership title

Developing a user-oriented interface for Splash, our video-mapping software

# Objective

This internship aims at improving the usability of [Splash](https://sat-mtl.gitlab.io/documentation/splash), our in-house, libre video-mapping software. The current user interface is developer-oriented, and as such very verbose and does not automate much of the process of video-mapping (except for parts like projectors calibration and blending).

The goal would be, using the same technology that is already in place (among which [Dear ImGui](https://github.com/ocornut/imgui/) which has become a standard for user interfaces in the videogame industry), to add a new simplified mode to the existing interface which would hide most of the complexity behind Splash. With the help of Metalab's team, this user interface will also be used to control Splash through the web.

# Tasks

- Participate in the ideation
- Participate in the maintainance of the software, particularly code related to the user interface
- Contribute to the source code of open source software
- Participate in the implementation of a new user interface
- Document the work and make demos/tutorials of it

# Context and software

- Multidisciplinary team
- Weekly meetings
- C++/Python/OpenGL/Linux
- Gitlab/Git
