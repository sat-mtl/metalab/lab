<!--
.. title: C++/Python/Linux development for networked audiovisual transmission
.. slug: dev-livre-networked-av
.. date: 2022-10-11
.. tags: internships
.. category: dev
.. link:
.. description:
.. type: closed 
.. author: Nicolas Bouillot
.. intern: 
.. mentors: Nicolas Bouillot
-->

# Internship title

C++/Python/Linux development for networked audiovisual transmission

# Objective

This internship aims to contribute to the development and implementation of Switcher, a software allowing networked low latency multichannel audiovisual transmission. The software is implemented in C++, with a Python wrapper. Different transmission protocols are implemented: SIP/RTP, RTMP, OSC and NDI. 

# Tasks

- Participate in the maintenance of the software through the open contribution process
- Participate in the backlog monitoring under gitlab
- Contribute to the source code of the open source software
- Participate in the implementation of Switcher in a multichannel transmission project with Metalab partners 
- Participate in the integration of Switcher in a tool architecture, with a view to deploying services in the Cloud

# Context and software

- Multidisciplinary team
- Weekly meetings
- C++/Python/CMake/Linux
- Network protocols for audiovisual transmission
- Command line and scripting
- Network measurement and monitoring tools
