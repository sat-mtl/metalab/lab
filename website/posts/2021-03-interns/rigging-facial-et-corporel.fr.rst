.. title: Animation / Rigging facial et corporel pour expérience immersive (LivePose)
.. slug: rigging-pour-experience-immersive
.. date: 2022-02-14
.. tags: stages, livepose
.. category:
.. link:
.. description:
.. type: filled
.. author: Marie-Eve Dumas
.. intern: Éliès Jurquet
.. mentors: Christian Frisson, Emmanuel Durand


Titre du stage
--------------
Animation / Rigging facial et corporel pour expérience immersive (LivePose)

Objectif
--------
Dans le cadre de ses recherches, le Metalab de la SAT développe `LivePose <https://gitlab.com/sat-metalab/livepose>`__, un outil en lien avec l'interactivité permettant la détection de la position du public et la détection de points-clés du corps et du visage à partir de flux vidéo issus de caméras.

En collaboration avec l'équipe du Metalab, vous produirez une démonstration incluant la capture en temps réel des expressions faciales et de la posture du corps d'un participant ainsi que sa représentation sous forme d'avatar dans un environnement virtuel.

Un premier stage sur ce sujet a permit d'explorer cette `thématique <https://sat.qc.ca/fr/nouvelles/workflow-de-capture-et-reproduction-de-visage-dans-blender>`__.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Expérimenter avec des avatars et leur squelette
* Modélisation et retouche d'avatar 3D
* Animation de modèle 3D du corps et du visage
* Participer à la vie du laboratoire : scrums, révision de code, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Blender / Python
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
