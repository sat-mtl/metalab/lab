.. title: Estimation de pose et détection d'actions par apprentissage profond
.. slug: action-detection
.. date: 2021-03-22
.. tags: stages
.. category:
.. link:
.. description:
.. type: closed


Titre du stage
--------------
Estimation de pose et détection d'actions par apprentissage profond

Objectif
--------
La création artistique s'intéresse depuis longtemps à l'interactivité entre le public et l'oeuvre.
Par l'intermédiaire de caméras et d'algorithmes d'intelligence artificielle, le Metalab développe un outil visant à ajouter une composante interactive à l'expérience artistique proposée par des créateurs. La composante interactive passe par la détection de la position du public et de points-clés du corps, permettant ainsi l'interaction en temps-réel avec des éléments visuels et sonores.

Ce stage a pour objectif de bonnifer la proposition interactive du Metalab par l'ajout de la détection d'actions qui permettra, par des actions de l'utilisateur, de contrôler, d'influencer et de transformer les éléments visuels et sonores; par exemple, pour créer un mouvement sur des particules, modifier la forme ou la position d'un objet par des gestes physiques.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Exploration des techniques d'estimation de pose et de détection d'actions pour notre cas d'usage
* Intégration d'outils basés sur des algorithmes d'intelligence artificielle et d'apprentissage automatique
* Travailler avec des flux vidéo issus de caméras RGB (webcams, caméras traditionnelles, mais aussi industrielles)
* Participer à la vie du laboratoire : scrums, révision de code, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Python
* Tensorflow, OpenCV
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
