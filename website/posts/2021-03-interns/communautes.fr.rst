.. title: Stratégies pour des communautés diverses, ouvertes et inclusives - logiciel libre
.. slug: foss-communities
.. date: 2021-03-22
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed

Titre du Stage
--------------

Stratégies pour une communauté informatique ouverte, diverse et inclusive - analyse pour les logiciels libres du Metalab

Objectif
--------

Dans le cadre de ses projets de recherche en immersion et en téléprésence, le Metalab crée, développe et entretient une suite d'outils logiciels publiés sous licence libre. Nous encourageons l'utilisation de nos outils dans des projets variés.

Nous souhaitons favoriser l'émergence et le développement d'une communauté qui soit ouverte, diverse et inclusive, de façon à faciliter l'utilisation de ces logiciels et afin de soutenir des contributions communautaires à ceux-ci.

L'objectif de ce stage est tout d'abord de procéder à un état des lieux des stratégies permettant d'assurer un environnement communautaire ouvert et inclusif. Par la suite, il s'agira d'analyser leur adéquation aux logiciels libres du Metalab. 

Ces stratégies seront recensées dans un rapport de recherche.

Tâches
------

Avec le soutien de l'équipe du Metalab, exploration des questions suivantes :

* Comment favoriser la création d'une communauté ouverte, diverse et inclusive ?
* Quelles actions concrètes le Metalab peut-il entreprendre afin d'encourager la participation à cette communauté ?

Avec le soutien de l'équipe du Metalab, réalisation d'un travail de recherche incluant :

* Explorer et documenter les pratiques communautaires adoptées dans l'univers des arts
* Explorer et documenter les pratiques communautaires adoptées dans l'univers des logiciels libres
* Effectuer des recommendations pour les outils d'animation de communauté
* Effectuer des recommandations pour le Metalab

Environnement de travail
------------------------

Notre environnement de travail favorise le travail en équipe et l'utilisation de logiciels libres, notamment par le biais de:

* Scrums et révisions de code
* GitLab, Python, Markdown, Bash
* Linux, outils libres
* LaTeX
