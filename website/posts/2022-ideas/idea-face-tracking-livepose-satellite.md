<!--
.. title: Face tracking for improving accessibility in hybrid telepresence interactions
.. slug: idea-face-tracking-livepose-satellite
.. author: Christian Frisson
.. date: 2022-02-18 01:16:11 UTC-05:00
.. tags: internships, ideas, hard, 350 hours, livepose, Satellite
.. type: filled
.. author: Christian Frisson
.. intern: Matthew Wiese
.. mentors: Christian Frisson, Farzaneh Askari, Emmanuel Durand
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Face tracking for improving accessibility in hybrid telepresence interactions

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

The [Satellite hub](https://hub.satellite.sat.qc.ca/about/) is an immersive 3D social web environment developed at SAT to promote various cultural and artistic contents and to create synergy by interconnecting them, started in 2020, in response to the health crisis. 

Satellite builds upon [Mozilla Hubs](https://github.com/mozilla/hubs/), a platform accessible from a web browser on a phone, tablet, computer or even a VR headset; and we want to improve its accessibility for people who have challenges in manipulating pointing devices, due to specific interaction needs or limited experience in browsing 3D spaces with 2D controls, following existing interest for [more natural conversations (via webcam eye, hand and facial expression tracking)](https://github.com/mozilla/hubs/issues/3689).

We have already prototyped experimental control of the Mozilla Hubs client with [LivePose](https://gitlab.com/sat-metalab/livepose/) ([video](https://vimeo.com/604196712)), our tool democratizing pose estimation by packaging bleeding-edge deep learning techniques and facilitating interoperability with networked multimedia authoring systems, and we want you to help us to push this concept further by designing and implementing camera-based interactions.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Experience the design space of eye and face pose estimation with [LivePose](https://gitlab.com/sat-metalab/livepose) and [MediaPipe](https://github.com/google/mediapipe).
* Integrate eye/face interaction techniques directly in [Mozilla Hubs](https://github.com/mozilla/hubs/) used for [Satellite](https://gitlab.com/sat-mtl/satellite/hubs-injection-server).
* (Bonus) Present our joint contributions at an international conference.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with JavaScript
* preferred: experience with Python
* preferred: understanding of the fundamentals of deep learning and computer vision

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/christianfrisson), [Emmanuel Durand](https://gitlab.com/paperManu), [Farzaneh Askari](https://gitlab.com/faskari)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
