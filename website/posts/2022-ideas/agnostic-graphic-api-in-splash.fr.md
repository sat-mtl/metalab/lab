<!--
.. title: Remplacer OpenGL par une bibliothèque de rendu graphique multi-API dans Splash
.. slug: idea-splash-rendering-api
.. author: Emmanuel Durand
.. date: 2022-10-11
.. tags: ideas, hard, 350 hours, internships, stages
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Titre du projet et description

<!-- Please add title below this comment to match the value in post title metadata -->

Le logiciel de vidéo *mapping* [Splash](https://sat-mtl.gitlab.io/documentation/splash) permet de contrôler conjointement plusieurs projecteurs vidéos de façon à obtenir une seule projection. Ce logiciel peut techniquement s'adapter à toute géométrie pour autant que sa surface soit diffuse. Son moteur de rendu utilise l'[API OpenGL](https://www.khronos.org/opengl/). Ce logiciel peut être utilisé sur toute plateforme Linux pouvant gérer un contexte OpenGL 4.5, et a été testé avec succès sur x86_64 (avec cartes graphiques NVIDIA, AMD et Intel) et aarch64 (avec carte graphique NVIDIA).

Cependant, pour pouvoir a) optimiser le logiciel et b) le rendre disponible sur d'autres plateformes, comme le Raspberry Pi, ce serait intéressant de pouvoir prendre en charge des APIs de rendu graphique supplémentaires. Pour ce faire, nous envisageons de remplacer l'utilisation directe de OpenGL par une bibliothèque intermédiaire, multi-API. Pour l'instant, [bfx](https://www.github.com/bkaradzic/bgfx) est en cours d'analyse.

## Description détaillée du projet

<!-- Please write 2-5 sentences below this comment --> 

Pour le moment, tout ce qui a trait au rendu graphique est fait par des appels directs à OpenGL, ce qui signifie que OpenGL est entrelacé avec le code source de Splash. Une première étape pourrait être d'isoler un peu plus les appels faits à OpenGL, ce qui nous faciliterait la tâche suivante, soit de remplacer ces appels par des appels à une bibliothèque de rendu multi-API.

## Résultats espérés

<!-- Please add 2-5 items below this comment --> 

* nettoyage du code de rendu de Splash
* ajout de la prise en charge d'au moins un API supplémentaire(Vulkan)
* possibilité d'exécuter Splash sur de nouvelles plateformes

## Compétences recherchées ou requises

<!-- Please add 2-5 items below this comment --> 

* C++
* OpenGL - la connaissances d'API supplémentaires est un atout
* expérience avec la programmation de moteurs de jeu ou de rendu

## Mentors possibles

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

Emmanuel Durand

## Durée envisagée pour le projet

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 heures

## Difficulté

<!-- Please write below this comment either: easy, medium or hard -->

Difficile
