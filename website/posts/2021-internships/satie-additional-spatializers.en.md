<!--
.. title: SATIE additional spatializers
.. slug: satie-additional-spatializers
.. date: 2022-10-11
.. tags: internships
.. category: audio
.. link:
.. description:
.. type: text
-->


# Internship title
Additional spatializer types for SATIE

# Objective

SATIE is our in-house audio spatializer that aims to be flexible, robust and supports many different arrangements of speaker arrays. This internship aims at bringing more spatialization techniques to SATIE.

# Tasks

- Participate in the ideation
- Review the literature that documents and discusses spatialization techniques
- Develop and test spatializer plugins for SATIE (using the SuperCollider framework)
- Participate in the development of a demo (video or live)
- Participate in the life of the laboratory: scrums, code review, etc.
- Document the work and ensure its reproducibility.

# Context and software

- Linux OS
- Digital signal processing
- SuperCollider, bash
- 3D audio and ambisonics
- VR/AR
- Optional: Python

# Contact

Please send your CV to metalab-stage@sat.qc.ca
