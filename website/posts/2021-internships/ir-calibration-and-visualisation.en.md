<!--
.. title: IR, Calibration and Visualisation
.. slug: ir-calibration-and-visualisation
.. date: 2022-10-11
.. tags: internships
.. category: audio
.. link:
.. description:
.. type: text
-->

# Internship title
Impulse Response, Calibration and Visualisation of Audio

# Objective

This interships aims to continue the development of recipes, approaches and tools around Impulse Response recording and visualisation. The purpose of this work is to identify some acoustic properties of a physical space to help with calibration of audio equipment.

# Tasks

- Participate in the ideation
- Explore existing in-house and other tools for similar tasks
- Experiment and develop or improve existing in-house tools or pipelines involving other tools
- Participate in the development of a demo (video or live)
- Participate in the life of the laboratory: scrums, code review, etc.
- Document the work and ensure its reproducibility.

# Context and software

- Linux OS
- Digital signal processing
- Python (numpy), bash
- 3D audio and ambisonics
- VR/AR
- Optional: SuperCollider

# Contact

Please send your CV to metalab-stage@sat.qc.ca
